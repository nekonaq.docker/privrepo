ARG BUILDPACK_VERSION
FROM buildpack-deps:$BUILDPACK_VERSION

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

ENV LANG C.UTF-8

WORKDIR /app

COPY secrets/env.deploy /tmp

RUN set -x \
&& . /tmp/env.deploy \
&& git clone "${GITLAB_PREFIX}nekonaq/authsample.git" authsample-gitlab \
&& git clone "${GITLAB_PREFIX}nekonaq/authsample.git" authsample-github \
&& ( set +x; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ) \
;
