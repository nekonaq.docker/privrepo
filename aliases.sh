export APP_DIR_PRIVREPO="$( /bin/pwd )"

@privrepo() {
  ( set -x
    cd "$APP_DIR_PRIVREPO"
    docker-compose "$@"
  )
}

@privrepo-devel() {
  @privrepo -f docker-compose-devel.yml "$@"
}

@privrepo.build() {
  @privrepo -f docker-compose.yml -f docker-compose-build.yml build "$@"
}

@privrepo.push() {
  ( . .env
    set -x
    docker push "$REGISTRY_PREFIX$COMPOSE_PROJECT_NAME:$IMAGE_VERSION"
  )
}

# usage:
#  @privrepo run --rm privrepo /bin/bash
#
#  @privrepo.build
#
#  @privrepo-devel run --rm privrepo /bin/bash
#  @privrepo-devel run --rm privrepo ls -la
